package br.ufc.great.ubiqua;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.fourthline.cling.binding.annotations.UpnpAction;
import org.fourthline.cling.binding.annotations.UpnpInputArgument;
import org.fourthline.cling.binding.annotations.UpnpOutputArgument;
import org.fourthline.cling.binding.annotations.UpnpService;
import org.fourthline.cling.binding.annotations.UpnpServiceId;
import org.fourthline.cling.binding.annotations.UpnpServiceType;
import org.fourthline.cling.binding.annotations.UpnpStateVariable;

@UpnpService(
		serviceId = @UpnpServiceId("SwitchPowerWithShell"), 
		serviceType = @UpnpServiceType(value = "SwitchPower", 
		version = 2)
)
public class SwitchPowerWithShell {

	@UpnpStateVariable(defaultValue = "0", sendEvents = false)
	private boolean target = false;

	@UpnpStateVariable(defaultValue = "0")
	private boolean status = false;

	@UpnpAction
	public void setTarget(@UpnpInputArgument(name = "NewTargetValue") boolean newTargetValue) {
		target = newTargetValue;
		status = newTargetValue;
		System.out.println("Switch is: " + status);
		System.out.println("executing shel");

		try {

			Process p = Runtime.getRuntime().exec("irsend SEND_ONCE CT-816 KEY_POWER");
			p.waitFor();

			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

			System.out.println(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@UpnpAction(out = @UpnpOutputArgument(name = "RetTargetValue"))
	public boolean getTarget() {
		return target;
	}

	@UpnpAction(out = @UpnpOutputArgument(name = "ResultStatus"))
	public boolean getStatus() {
		// If you want to pass extra UPnP information on error:
		// throw new ActionException(ErrorCode.ACTION_NOT_AUTHORIZED);
		return status;
	}
}
