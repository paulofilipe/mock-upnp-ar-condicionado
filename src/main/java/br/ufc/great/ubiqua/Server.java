package br.ufc.great.ubiqua;

import java.io.IOException;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.binding.LocalServiceBindingException;
import org.fourthline.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.fourthline.cling.model.DefaultServiceManager;
import org.fourthline.cling.model.ValidationException;
import org.fourthline.cling.model.meta.DeviceDetails;
import org.fourthline.cling.model.meta.DeviceIdentity;
import org.fourthline.cling.model.meta.LocalDevice;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.meta.ManufacturerDetails;
import org.fourthline.cling.model.meta.ModelDetails;
import org.fourthline.cling.model.types.DeviceType;
import org.fourthline.cling.model.types.UDADeviceType;
import org.fourthline.cling.model.types.UDN;

public class Server implements Runnable{

	public static void main(String[] args) throws Exception {
		// Start a user thread that runs the UPnP stack
		Thread serverThread = new Thread(new Server());
		serverThread.setDaemon(false);
		serverThread.start();
	}

	public void run() {
		try {

			final UpnpService upnpService = new UpnpServiceImpl();

			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					upnpService.shutdown();
				}
			});

			// Add the bound local device to the registry
			upnpService.getRegistry().addDevice(createDevice("Abajour", false));
			upnpService.getRegistry().addDevice(createDevice("microwave", false));
			upnpService.getRegistry().addDevice(createDevice("television", true));
			upnpService.getRegistry().addDevice(createDevice("refrigerator", false));
			upnpService.getRegistry().addDevice(createDevice("television", false));
			upnpService.getRegistry().addDevice(createDevice("air-conditioning", false));

		} catch (Exception ex) {
			System.err.println("Exception occured: " + ex);
			ex.printStackTrace(System.err);
			System.exit(1);
		}
	}

	// DOC: CREATEDEVICE
	LocalDevice createDevice(String friendlyName, boolean shell )
			throws ValidationException, LocalServiceBindingException, IOException {

		DeviceIdentity identity = new DeviceIdentity(UDN.uniqueSystemIdentifier("Demo Binary " + friendlyName));

		DeviceType type = new UDADeviceType(friendlyName, 1);

		DeviceDetails details = new DeviceDetails(friendlyName, new ManufacturerDetails("ACME"), new ModelDetails(
				"Binary" + friendlyName + "2000", "A simple " + friendlyName + " with on/off switch.", "v1"));

		if (shell) {
			LocalService<SwitchPowerWithShell> switchPowerService = new AnnotationLocalServiceBinder().read(SwitchPowerWithShell.class);
			switchPowerService.setManager(new DefaultServiceManager(switchPowerService, SwitchPowerWithShell.class));
			return new LocalDevice(identity, type, details, switchPowerService);
		} else {
			LocalService<SwitchPower> switchPowerService = new AnnotationLocalServiceBinder().read(SwitchPower.class);
			switchPowerService.setManager(new DefaultServiceManager(switchPowerService, SwitchPower.class));
			return new LocalDevice(identity, type, details, switchPowerService);
		}
	}
}
